import { App } from './src';
export const app = new App();
import { config } from 'dotenv';
config();
app
  .init()
  .then(() => {
    return app.createServer();
  })
  .then((server) => {
    server.setTimeout(180000, () => {
      // console.log('Server timedout')
    });
  });