import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  Index,
  ManyToOne,
  Unique,
  BaseEntity,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({
  name: "COUNTRIES"
})
export class Country extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: "country_name",
    type: "char",
    length: 255
  })
  countryName: string

  @Column({
    name: "currency",
    type:"char",
    length: 4
  })
  currency: string

  @Column({
    name:"population",
    type:"integer",
    nullable: true
  })
  population: number
}