import { Application, RequestHandler, Router } from 'express';
import { CountryController } from 'src/controllers';
import {App} from '../index'

export class Country {
    static route = 'country';
    protected _router: Router = Router();
    static isRoute = true;
    constructor(protected app:App) {
        console.log("Initializing the route:",this.constructor["route"])
    }
    init() {
        this._router.get('',this.getCountry)
        this._router.get('/:country_name',this.getCountry)
        return this._router
    }

    getCountry:RequestHandler = (req,res) => {
        console.log(req.params.country_name)
        if(req.params.country_name) 
        this.app.controllers["CountryController"].getCountry({ country_name: req.params.country_name })
            .then(d => {
                console.log(d)
                res.json(d)
            })
            .catch(e => {
                res.json({
                    error: e.toString()
                })
            })
        else this.app.controllers["CountryController"].getCountries()
            .then(d => res.json(d))
            .catch(e => {
                res.sendStatus(500)
                res.json({
                    error: e.toString()
                })
            })
    }
}