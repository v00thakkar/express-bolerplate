import { RequestHandler } from 'express';
import { Country } from '../models/country.model'
import {App} from '../index'
export class CountryController {
    static controller_name = "CountryController"
    constructor(private app: App) {

    }
    getCountry = (obj: IInputGetCountry) => {
        /**
         * Add joi for validation
         */
        let { country_name } = obj;
        return Country.findOneOrFail({
            where: { 
                countryName: country_name
             }
        }).then((d) => {
            return d
        })

    }
    getCountries = (obj: IInputGetCountries) => {
        return Country.find()
            .then((d) => {
                return d
            })
    }
}

interface IInputGetCountries {

}
interface IInputGetCountry {
    country_name: string
}