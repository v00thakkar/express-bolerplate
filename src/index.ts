import { EventEmitter } from 'events';
import { Application } from 'express';
import express from 'express';
import * as bodyParser from 'body-parser';
import { createConnection, Connection } from 'typeorm';

import * as routes from './routes'
import * as controllers from './controllers'

export class App extends EventEmitter {
    protected app: Application = express();
    connection: Connection;
    controllers = {};
    adaptors = {      
    }
    constructor() {
        super();
    }
    async init() {
        /**
         * Set middlewares
         */
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(bodyParser.json());
        /**
         * Setup adaptors
         */
        this.connection = await createConnection({
            type: "mysql",
            host: process.env.MYSQL_HOST,
            username: process.env.MYSQL_USER,
            password: process.env.MYSQL_PASS,
            database: process.env.MYSQL_DB,
            //insecureAuth:true,
            entities: [__dirname + '/models/*']
        })

        for (let i in controllers) {
            this.controllers[controllers[i].controller_name] = new controllers[i](this)
        }

        for (let i in routes) {
            if (routes[i].isRoute) {
                this.app.use(`/api/${routes[i].route}`, new routes[i](this).init())
            }
        }
    }
    async createServer() {
        console.log('Starting server on ', process.env.PORT || 3000);
        return this.app.listen(process.env.PORT || 3000, (...args) => {
            console.log(args);
        });
    }
}